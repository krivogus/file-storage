import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { FileStorageModule } from './file-storage/file-storage.module';

@Module({
  imports: [FileStorageModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
