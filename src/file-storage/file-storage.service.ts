import { Injectable, HttpStatus } from '@nestjs/common';
import { IResponseDTO, ISendFileDTO } from './file-storage.dto';
import { promises, exists, mkdir } from 'fs';
import { promisify } from 'util';
import { join } from 'path';

const existsAsync = promisify(exists);
const mkdirAsync = promisify(mkdir);

@Injectable()
export class FileStorageService {
  private readonly staticDir: string = join(__dirname, process.env.STATIC_DIR);

  async onModuleInit() {
    if (!(await existsAsync(this.staticDir))) {
      await mkdirAsync(this.staticDir);
    }
  }

  async saveFile(file: ISendFileDTO): Promise<IResponseDTO> {
    const filePath = join(this.staticDir, file.fileId);
    const fileBuffer = Buffer.from(file.buffer.data);

    try {
      await promises.writeFile(filePath, fileBuffer, 'binary');
    } catch {
      return {
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
        message: 'Internal Server Error',
      };
    }
    return { statusCode: HttpStatus.OK, message: 'OK' };
  }

  async getFile(fileId: string): Promise<any | IResponseDTO> {
    const filePath = join(this.staticDir, fileId);

    try {
      return promises.readFile(filePath, 'binary');
    } catch {
      return {
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
        message: 'Internal Server Error',
      };
    }
  }
}
