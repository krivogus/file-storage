import { HttpStatus } from '@nestjs/common';

export interface IFileDTO {
  readonly fieldname: string;
  readonly originalname: string;
  readonly encoding: string;
  readonly mimetype: string;
  readonly buffer: {
    type: 'Buffer';
    data: number[];
  };
  readonly size: number;
}

export interface ISendFileDTO extends IFileDTO {
  fileId: string;
}

export interface IResponseDTO {
  readonly statusCode: HttpStatus;
  readonly message: string;
}

export interface IGetFileDTO {
  readonly fileId: string;
}
