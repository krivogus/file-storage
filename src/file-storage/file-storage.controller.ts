import { Controller, Header, Res } from '@nestjs/common';
import { FileStorageService } from './file-storage.service';
import { MessagePattern } from '@nestjs/microservices';
import { IResponseDTO, ISendFileDTO, IGetFileDTO } from './file-storage.dto';

@Controller()
export class FileStorageController {
  private readonly fileStorageService: FileStorageService;

  constructor() {
    this.fileStorageService = new FileStorageService();
  }

  @MessagePattern({ file: 'post' })
  async sendFile(file: ISendFileDTO): Promise<IResponseDTO> {
    return await this.fileStorageService.saveFile(file);
  }

  @MessagePattern({ file: 'get' })
  @Header('Content-Type', 'image/*')
  async getFile({ fileId }: IGetFileDTO): Promise<any | IResponseDTO> {
    return await this.fileStorageService.getFile(fileId);
  }
}
